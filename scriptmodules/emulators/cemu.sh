#!/usr/bin/env bash

# This file is part of ARES by The RetroArena
#
# ARES is the legal property of its developers, whose names are
# too numerous to list here. Please refer to the COPYRIGHT.md file distributed with this source.
#
# See the LICENSE.md file at the top-level directory of this distribution and
# at https://raw.githubusercontent.com/Retro-Arena/RetroArena-Setup/master/LICENSE.md
#
#

rp_module_id="Cemu"
rp_module_desc="A wiiu emulator"
rp_module_help="ROM Extensions: .rpx\n\nCopy your Wii U roms to $romdir/wiiu"
rp_module_licence="BSD https://github.com/jpd002/Play-/blob/master/License.txt"
rp_module_section="sa"
rp_module_flags="!all !arm 64bit"

function depends_cemu() {

if isPlatform "rpi"; then
        if ! rp_isInstalled "wine64" ; then
            md_ret_errors+=("Sorry, you need to install the Wine64 scriptmodule")
            return 1
        fi
    fi

function setup_cemu() {
    mkdir ~/.wineCemu
    export WINEARCH = win64
    export WINEPREFIX = "/$HOME/.wineCemu"
    xset -dpms s off s noblank
    matchbox-window-manager &
    WINEDEBUG=-all LD_LIBRARY_PATH="/opt/retropie/supplementary/mesa/lib/" setarch linux64 -L /opt/retropie/ports/wine64/bin/wine64 ~/ares-setup-arm-64/scriptmodules/emulators/cemu/dependencies/vc_redist.x64.exe / desktop=shell,\`xrandr | grep current | sed 's/.*current //; s/,.*//; s/ //g'\`

}


function configure_cemu() {
    mkRomDir "wiiu"
    sudo mv -f ~/ares-setup-arm-64/scriptmodules/emulators/cemu/ /opt/retropie/emulators/cemu
    
    cat > "$cemu_xinit" << _EOFDESKTOP_
#!/bin/bash
xset -dpms s off s noblank
matchbox-window-manager &
WINEDEBUG=-all LD_LIBRARY_PATH="/opt/retropie/supplementary/mesa/lib/" setarch linux64 -L /opt/retropie/ports/wine64/bin/wine64 /opt/retropie/emulators/cemu/CemuLauncher.exe desktop=shell,\`xrandr | grep current | sed 's/.*current //; s/,.*//; s/ //g'\`
_EOFDESKTOP_

chmod +x "$cemu_xinit"

    addEmulator 1 "$md_id" "wiiu" "$md_inst cemu_xinit -g  ~/ARES/roms/wiiu/%ROM%"
    addSystem "wiiu"
}
