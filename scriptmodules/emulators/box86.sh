#!/usr/bin/env bash

# This file is part of ARES by The RetroArena
#
# ARES is the legal property of its developers, whose names are
# too numerous to list here. Please refer to the COPYRIGHT.md file distributed with this source.
#
# See the LICENSE.md file at the top-level directory of this distribution and
# at https://raw.githubusercontent.com/Retro-Arena/RetroArena-Setup/master/LICENSE.md
#
# Core script functionality is based upon The RetroPie Project https://retropie.org.uk Script Modules
#
#Script made by georgemcmullen

rp_module_id="box86"
rp_module_desc="Box86 emulator"
rp_module_help="Place your x86 binaries $romdir/box86"
rp_module_licence="MIT https://github.com/ptitSeb/box86/blob/master/LICENSE"
rp_module_section="exp"
rp_module_flags=""

function _latest_ver_box86() {
    # This defines the Git tag / branch which will be used. Main repository is at:
    # https://github.com/ptitSeb/box86
    echo master
    # The following is not working yet. Releases must be non-prerelease and non-draft.
    # wget -qO- https://api.github.com/repos/ptitSeb/box86/releases/latest | grep -m 1 tag_name | cut -d\" -f4
}

function depends_box86() {
    if compareVersions $__gcc_version lt 7; then
        md_ret_errors+=("Sorry, you need an OS with gcc 7 or newer to compile $md_id")
        return 1
    fi

    if compareVersions $__version lt 4.7.7; then
        md_ret_errors+=("Sorry, you need to be running RetroPie v4.7.7 or later")
        return 1
    fi

    if ! rp_isInstalled "mesa" ; then
        md_ret_errors+=("Sorry, you need to install the Mesa scriptmodule")
        return 1
    fi
    
    #add architecture armhf
    sudo dpkg --add-architecture armhf
    
    # Install required libraries required for compilation and running
    getDepends binfmt-support cmake gtk2-engines-murrine libncurses5 libncursesw5 libssl1.0.2 libglu1-mesa zenity mesa-utils libinput10 libxkbcommon-x11-0 matchbox-window-manager xorg gcc-arm-linux-gnueabihf libllvm12:armhf linux-libc-dev:armhf git cmake libncurses6:armhf libc6:armhf  libx11-6:armhf libgdk-pixbuf2.0-0:armhf libgtk2.0-0:armhf libstdc++6:armhf libsdl2-2.0-0:armhf mesa-va-drivers:armhf libsdl1.2-dev:armhf libsdl-mixer1.2:armhf libpng16-16:armhf libcal3d12v5:armhf libsdl2-net-2.0-0:armhf libopenal1:armhf libsdl2-image-2.0-0:armhf libvorbis-dev:armhf libcurl4:armhf libjpeg62:armhf  libudev1:armhf libgl1-mesa-dev:armhf  libx11-dev:armhf libsmpeg0:armhf libavcodec58:armhf libavformat58:armhf libswscale5:armhf libsdl2-image-2.0-0:armhf libsdl2-mixer-2.0-0:armhf gcc-arm-linux-gnueabihf cmake git cabextract libavutil56:armhf libswresample3:armhf libavutil56:armhf libchromaprint1:armhf libavutil56:armhf libvdpau1:armhf

    # Restarting the binfmt service should eliminate the need to reboot the machine after installation.
    systemctl restart systemd-binfmt
    
    # X11 on RPi is currently using VMWare's LLVM GL Driver for some reason. That should be removed.
    # Recommended as per: https://www.raspberrypi.org/forums/viewtopic.php?t=196423
    apt remove -y xserver-xorg-video-fbturbo
}

function dependenciesFix {
	 sudo mv /usr/share/doc/linux-libc-dev/changelog.Debian.gz /usr/share/doc/linux-libc-dev/changelog.Debian.gz.old 
	 sudo rm /usr/include/drm/drm_fourcc.h
	 sudo rm /usr/include/drm/lima_drm.h
	 sudo apt -y --fix-broken install
}

function sources_box86() {
    gitPullOrClone "$md_build" https://github.com/ptitSeb/box86.git "$(_latest_ver_box86)"
}

function build_box86() {
    mkdir build
    cd build
    cmake .. -DARM_DYNAREC=1 -DRPI4ARM64=1 -DCMAKE_BUILD_TYPE=RelWithDebInfo
    make -j4
    cd ..
}

function install_box86() {
    md_ret_files=(
        'build/box86'
        'build/libdynarec.a'
        'docs/LICENSE'
    )
}

function configure_box86() {
    local system="box86"

    update-binfmts --install i386 "$md_inst/${system}" --magic '\x7fELF\x01\x01\x01\x03\x00\x00\x00\x00\x00\x00\x00\x00\x03\x00\x03\x00\x01\x00\x00\x00' --mask '\xff\xff\xff\xff\xff\xff\xff\xfc\xff\xff\xff\xff\xff\xff\xff\xff\xf8\xff\xff\xff\xff\xff\xff\xff'
}
