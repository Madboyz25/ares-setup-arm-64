#!/usr/bin/env bash

# This file is part of ARES by The RetroArena
#
# ARES is the legal property of its developers, whose names are
# too numerous to list here. Please refer to the COPYRIGHT.md file distributed with this source.
#
# See the LICENSE.md file at the top-level directory of this distribution and
# at https://raw.githubusercontent.com/Retro-Arena/RetroArena-Setup/master/LICENSE.md
#
#

rp_module_id="xemu"
rp_module_desc="Original Xbox emulator"
rp_module_help="ROM Extensions: .iso\n\nCopy your Xbox roms to $romdir/xbox"
rp_module_licence="BSD https://github.com/mborgerson/xemu/blob/master/LICENSE"
rp_module_section="sa"
rp_module_flags="!all !arm 64bit"

function depends_xemu() {
    getDepends git build-essential libsdl2-dev libepoxy-dev libpixman-1-dev libgtk-3-dev libssl-dev libsamplerate0-dev libpcap-dev ninja-build
	
	if isPlatform "odroid-n2"; then
	/home/aresuser/ARES-Setup/fixmali.sh
    elif isPlatform "rockpro64"; then
    /usr/lib/arm-linux-gnueabihf/install_mali.sh
	fi
}

function sources_xemu() {
    local branch="master"
    gitPullOrClone "$md_build" https://github.com/mborgerson/xemu "$branch" 

}

function build_xemu() {
    bash /tmp/build/xemu/build.sh
}

function configure_xemu() {
    mkRomDir "xbox"
    addEmulator 1 "$md_id" "xbox" "$md_inst/bin/xemu -full-screen -dvd_path %ROM%"
    addSystem "xbox"
}
