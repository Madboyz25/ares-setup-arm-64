#!/usr/bin/env bash

# This file is part of ARES by The RetroArena
#
# ARES is the legal property of its developers, whose names are
# too numerous to list here. Please refer to the COPYRIGHT.md file distributed with this source.
#
# See the LICENSE.md file at the top-level directory of this distribution and
# at https://raw.githubusercontent.com/Retro-Arena/RetroArena-Setup/master/LICENSE.md

#

rp_module_id="citra"
rp_module_desc="3ds emulator"
rp_module_help="ROM Extension: .3ds\n\nCopy your 3DS roms to  $romdir/3ds"
rp_module_licence="GPL2 https://github.com/citra-emu/citra/blob/master/license.txt"
rp_module_section="sa"
rp_module_flags="!arm !odroid-n2 64bit"

function depends_citra() {
    if compareVersions $__gcc_version lt 7; then
        md_ret_errors+=("Sorry, you need an OS with gcc 7.0 or newer to compile citra")
        return 1
    fi

    # Additional libraries required for running
    local depends=(libsdl2-dev doxygen qtbase5-dev libqt5opengl5-dev qtmultimedia5-dev build-essential clang clang-format libc++-dev cmake qtbase5-dev libqt5opengl5-dev qtmultimedia5-dev)
    getDepends "${depends[@]}"
	
	if isPlatform "odroid-n2"; then
	/home/aresuser/ARES-Setup/fixmali.sh
    elif isPlatform "rockpro64"; then
    /usr/lib/arm-linux-gnueabihf/install_mali.sh
	fi
}

function sources_citra() {
    gitPullOrClone "$md_build" https://github.com/slaminger/citra-android
}

function build_citra() {
    cd "$md_build/citra"
    mkdir build
    cd build
    cmake .. -DCMAKE_BUILD_TYPE=Release -DENABLE_QT=ON #for now QT_BUILD disable doesn't compile missing header due the packages for debian/ubuntu doesn't support opengl desktop 3.3 core.
    make
    md_ret_require="$md_build/build/bin/Release"

}

function install_citra() {
      md_ret_files=(
      '/build/bin/Release/citra'
     '/build/bin/Release/citra-qt' 
      
      )

}

function configure_citra() {

    mkRomDir "3ds"
    addEmulator 1 "$md_id" "3ds" "$md_inst/citra %ROM%"
    addEmulator 0 "${md_id}-fullscreen" "3ds" "$md_inst/citra -f %ROM%"
   # addEmulator 0 "$md_id-qt" "3ds" "$md_inst/citra-qt %ROM%"
    addSystem "3ds"

}
