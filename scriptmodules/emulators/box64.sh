#!/usr/bin/env bash

# This file is part of ARES by The RetroArena
#
# ARES is the legal property of its developers, whose names are
# too numerous to list here. Please refer to the COPYRIGHT.md file distributed with this source.
#
# See the LICENSE.md file at the top-level directory of this distribution and
# at https://raw.githubusercontent.com/Retro-Arena/RetroArena-Setup/master/LICENSE.md
#
# Core script functionality is based upon The RetroPie Project https://retropie.org.uk Script Modules
#
#Script made by george

rp_module_id="box64"
rp_module_desc="Box64 emulator"
rp_module_help="Place your x64 binaries $romdir/box64"
rp_module_licence="MIT https://github.com/ptitSeb/box86/blob/master/LICENSE"
rp_module_section="exp"
rp_module_flags=""

function _latest_ver_box86() {
    # This defines the Git tag / branch which will be used. Main repository is at:
    # https://github.com/ptitSeb/box86
    echo master
    # The following is not working yet. Releases must be non-prerelease and non-draft.
    # wget -qO- https://api.github.com/repos/ptitSeb/box86/releases/latest | grep -m 1 tag_name | cut -d\" -f4
}

function depends_box64() {
    
    if ! rp_isInstalled "mesa" ; then
        md_ret_errors+=("Sorry, you need to install the Mesa scriptmodule")
        return 1
    fi
    
    # Install required libraries required for compilation and running
    getDepends linux-libc-dev build-essential

    # Restarting the binfmt service should eliminate the need to reboot the machine after installation.
    systemctl restart systemd-binfmt
    
    # X11 on RPi is currently using VMWare's LLVM GL Driver for some reason. That should be removed.
    # Recommended as per: https://www.raspberrypi.org/forums/viewtopic.php?t=196423
    apt remove -y xserver-xorg-video-fbturbo
}

function sources_box64() {
    gitPullOrClone "$md_build" https://github.com/ptitSeb/box64.git "$(_latest_ver_box64)"
}

function build_box64() {
    mkdir build
    cd build
    cmake .. -DARM_DYNAREC=1 -DRPI4ARM64=1 -DCMAKE_BUILD_TYPE=RelWithDebInfo
    make -j4
    sudo make install
    cd ..
}

function install_box64() {
    md_ret_files=(
        'build/box64'
        'build/libd/narec.a'
        'docs/LICENSE'
    )
}

function configure_box64() {
    local system="box64"
