#!/usr/bin/env bash

# This file is part of ARES by The RetroArena
#
# ARES is the legal property of its developers, whose names are
# too numerous to list here. Please refer to the COPYRIGHT.md file distributed with this source.
#
# See the LICENSE.md file at the top-level directory of this distribution and
# at https://raw.githubusercontent.com/Retro-Arena/RetroArena-Setup/master/LICENSE.md
#
#

rp_module_id="lightspark"
rp_module_desc="lightspark emulator play flash"
rp_module_help="ROM Extensions: .swf\n\nCopy your flash roms to $romdir/flash"
rp_module_licence="LGPL-3.0 https://github.com/lightspark/lightspark/blob/master/COPYING"
rp_module_section="sa"
rp_module_flags="!all !arm 64bit"

function depends_lightspark() {
    getDepends git gcc g++ nasm cmake gettext libcurl4-gnutls-dev libsdl2-mixer-dev libsdl2-dev libpango1.0-dev libcairo2-dev libavcodec-dev libavresample-dev libglew-dev librtmp-dev libjpeg-dev libavformat-dev liblzma-dev
	
	if isPlatform "odroid-n2"; then
	/home/aresuser/ARES-Setup/fixmali.sh
    elif isPlatform "rockpro64"; then
    /usr/lib/arm-linux-gnueabihf/install_mali.sh
	fi
}

function sources_lightspark() {
    local branch="master"
    gitPullOrClone "$md_build" https://github.com/lightspark/lightspark "$branch"

}

function build_lightspark() {
    cmake -DCMAKE_BUILD_TYPE=Release ..
    make clean
    make
}

function install_lightspark() {
    make install
}

function configure_lightspark() {
    mkRomDir "flash"
    moveConfigDir "$home/.lightspark" "$md_conf_root/flash"
    addEmulator 1 "$md_id" "flash" "$md_inst/bin/lightspark %ROM%"
    addSystem "flash"
}
