#!/usr/bin/env bash

# This file is part of ARES by The RetroArena
#
# ARES is the legal property of its developers, whose names are
# too numerous to list here. Please refer to the COPYRIGHT.md file distributed with this source.
#
# See the LICENSE.md file at the top-level directory of this distribution and
# at https://raw.githubusercontent.com/Retro-Arena/RetroArena-Setup/master/LICENSE.md
#
# Core script functionality is based upon The RetroPie Project https://retropie.org.uk Script Modules
#

rp_module_id="lr-dolphin"
rp_module_desc="Gamecube/Wii emulator - Dolphin port for libretro"
rp_module_help="ROM Extensions: .gcm .iso .wbfs .ciso .gcz\n\nCopy your gamecube roms to $romdir/gc Triforce roms to $romdir/triforce and Wii roms to $romdir/wii"
rp_module_licence="GPL2 https://raw.githubusercontent.com/libretro/dolphin/master/license.txt"
rp_module_section="lr"
rp_module_flags="!arm !odroid-n2 64bit "

function depends_lr-dolphin() {
    depends_dolphin
	
	if isPlatform "odroid-n2"; then
	/home/aresuser/ARES-Setup/fixmali.sh
    elif isPlatform "rockpro64"; then
    /usr/lib/arm-linux-gnueabihf/install_mali.sh
	fi
}

function sources_lr-dolphin() {
    gitPullOrClone "$md_build" https://github.com/libretro/dolphin
}

function build_lr-dolphin() {
    mkdir build
    cd build
    cmake .. -DLIBRETRO=ON -DLIBRETRO_STATIC=1
    make clean
    make
    md_ret_require="$md_build/build/dolphin_libretro.so"
}

function install_lr-dolphin() {
    md_ret_files=(
        'build/dolphin_libretro.so'
    )
}

function configure_lr-dolphin() {
    mkRomDir "gc"
    mkRomDir "triforce"
    mkRomDir "wii"

    ensureSystemretroconfig "gc"
    ensureSystemretroconfig "triforce"
    ensureSystemretroconfig "wii"

    addEmulator 1 "$md_id" "gc" "$md_inst/dolphin_libretro.so"
    addEmulator 1 "$md_id" "triforce" "$md_inst/dolphin_libretro.so"
    addEmulator 1 "$md_id" "wii" "$md_inst/dolphin_libretro.so"

    addSystem "gc"
    addSystem "triforce"
    addSystem "wii"
}
