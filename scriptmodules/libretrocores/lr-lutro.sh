#!/usr/bin/env bash

# This file is part of ARES by The RetroArena
#
# ARES is the legal property of its developers, whose names are
# too numerous to list here. Please refer to the COPYRIGHT.md file distributed with this source.
#
# See the LICENSE.md file at the top-level directory of this distribution and
# at https://raw.githubusercontent.com/Retro-Arena/RetroArena-Setup/master/LICENSE.md
#
# Core script functionality is based upon The RetroPie Project https://retropie.org.uk Script Modules
#

rp_module_id="lr-lutro"
rp_module_desc="Lua engine - lua game framework (WIP) for libretro following the LÖVE API"
rp_module_help="ROM Extensions: .lutro .lua"
rp_module_licence="GPL3 https://raw.githubusercontent.com/libretro/libretro-lutro/master/LICENSE"
rp_module_section="lr"
rp_module_flags="!aarch64"

function sources_lr-lutro() {
    gitPullOrClone "$md_build" https://github.com/libretro/libretro-lutro.git
}

function build_lr-lutro() {
    make clean
    make 
    md_ret_require="$md_build/lutro_libretro.so"
}

function install_lr-lutro() {
    md_ret_files=(
	'lutro_libretro.so'
    )
}

function configure_lr-lutro() {
    	
    addEmulator 1 "$md_id" "lutro" "$md_inst/lutro_libretro.so"
    addSystem "lutro"
	
    mkRomDir "lutro"
    ensureSystemretroconfig "lutro"
}