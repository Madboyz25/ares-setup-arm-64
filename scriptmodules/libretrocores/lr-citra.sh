#!/usr/bin/env bash

# This file is part of ARES by The RetroArena
#
# ARES is the legal property of its developers, whose names are
# too numerous to list here. Please refer to the COPYRIGHT.md file distributed with this source.
#
# See the LICENSE.md file at the top-level directory of this distribution and
# at https://raw.githubusercontent.com/Retro-Arena/RetroArena-Setup/master/LICENSE.md
#
# Core script functionality is based upon The RetroPie Project https://retropie.org.uk Script Modules
#

rp_module_id="lr-citra"
rp_module_desc="3DS emulator - Citra port for libretro"
rp_module_help="ROM Extensions: .gcm .iso .wbfs .ciso .gcz\n\nCopy your 3DS roms to $romdir/3ds"
rp_module_licence="GPL2 https://raw.githubusercontent.com/libretro/dolphin/master/license.txt"
rp_module_section="lr"
rp_module_flags="!arm !odroid-n2 64bit "

function depends_lr-citra() {
    depends_citra
	
	if isPlatform "odroid-n2"; then
	/home/aresuser/ARES-Setup/fixmali.sh
    elif isPlatform "rockpro64"; then
    /usr/lib/arm-linux-gnueabihf/install_mali.sh
	fi
}

function sources_lr-citra() {
    gitPullOrClone "$md_build" https://github.com/libretro/citra
}

function build_lr-citra() {
    mkdir build
    cd build
    cmake .. -DLIBRETRO=ON -DLIBRETRO_STATIC=1
    make clean
    make
    md_ret_require="$md_build/build/citra_libretro.so"
}

function install_lr-citra() {
    md_ret_files=(
        'build/citra_libretro.so'
    )
}

function configure_lr-citra() {
    mkRomDir "3ds"

    ensureSystemretroconfig "3ds"

    addEmulator 1 "$md_id" "3ds" "$md_inst/citra_libretro.so"

    addSystem "3ds"
}
