#!/usr/bin/env bash

# This file is part of ARES by The RetroArena
#
# ARES is the legal property of its developers, whose names are
# too numerous to list here. Please refer to the COPYRIGHT.md file distributed with this source.
#
# See the LICENSE.md file at the top-level directory of this distribution and
# at https://raw.githubusercontent.com/Retro-Arena/RetroArena-Setup/master/LICENSE.md
#
# Core script functionality is based upon The RetroPie Project https://retropie.org.uk Script Modules
#

rp_module_id="devilutionx"
rp_module_desc="devilutionx a Diablo 1 engine"
rp_module_help="Place your diabdat.mpq inside the $romdir/ports/devilutionx/ folder"
rp_module_licence="The UNlicense https://raw.githubusercontent.com/diasurgical/devilutionX/master/LICENSE"
rp_module_section="prt"
rp_module_flags=""

function install_bin_devilutionx() {
    if  isPlatform "odroid-xu" || isPlatform "odroid-n2"; then
	aptInstall devilutionx
	elif isPlatform "rockpro64" || isPlatform  "rpi"; then
	downloadAndExtract "https://github.com/Retro-Arena/ARES-Binaries/raw/master/buster/odroid-xu/ports/devilutionx.tar.gz" "$md_inst" --strip-components 1
	fi
	
}

function configure_devilutionx() {
   if  isPlatform "odroid-xu" || isPlatform "odroid-n2"; then
   mkRomDir "ports/$md_id"
    ln -sfn /home/aresuser/ARES/roms/ports/devilutionx/diabdat.mpq /home/aresuser/.local/share/diasurgical/devilution/diabdat.mpq
    addPort "$md_id" "devilutionx" "devilutionx" /usr/share/games/diablo/devilutionx
    chown -R $user:$user /usr/share/games/diablo/
    chmod -R 755 /usr/share/games/diablo/
	elif isPlatform "rockpro64" || isPlatform  "rpi"; then
	 mkRomDir "ports/$md_id"
    ln -sfn /home/aresuser/ARES/roms/ports/devilutionx/diabdat.mpq "$md_inst/diabdat.mpq"
    addPort "$md_id" "devilutionx" "devilutionx" "$md_inst/devilutionx"
    chown -R $user:$user "$md_inst"
    chmod -R 755 "$md_inst"
	fi
}
